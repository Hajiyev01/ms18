package az.ingress.demo.repositery;

import az.ingress.demo.model.Teacher;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeacherRepositery extends JpaRepository<Teacher,Integer> {



}
