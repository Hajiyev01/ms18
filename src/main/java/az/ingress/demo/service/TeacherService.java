package az.ingress.demo.service;

import az.ingress.demo.model.Teacher;
import az.ingress.demo.repositery.TeacherRepositery;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


public interface TeacherService {


    public Teacher getById(int id) ;

    public Teacher saveTeacher(Teacher teacher) ;


    public Teacher updateTeacher(Teacher teacher);

    public void deleteTeacher(int id) ;
}
