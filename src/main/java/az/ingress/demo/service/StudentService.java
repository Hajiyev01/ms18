package az.ingress.demo.service;

import az.ingress.demo.model.Student;
import az.ingress.demo.repositery.StudentRepositery;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class StudentService {
    private final StudentRepositery studentRepositery;


    public Student getStudentById(int id) {
        return studentRepositery.findById(id).get();
    }

    public Student saveStudent(Student student) {
        return studentRepositery.save(student);
    }

    public Student updateStudent(Student student) {
        return studentRepositery.save(student);
    }

    public void deleteStudent(int id) {
        studentRepositery.deleteById(id);
    }
}
