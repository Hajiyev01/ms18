package az.ingress.demo.service.serviceImpl;

import az.ingress.demo.model.Teacher;
import az.ingress.demo.repositery.TeacherRepositery;
import az.ingress.demo.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@RequiredArgsConstructor
@Service
public class TeacherServiceImpl implements TeacherService {

  private final TeacherRepositery teacherRepositery;
    @Override
    public Teacher getById(int id) {
        return teacherRepositery.findById(id).get();
    }

    @Override
    public Teacher saveTeacher(Teacher teacher) {
        return teacherRepositery.save(teacher);
    }

    @Override
    public Teacher updateTeacher(Teacher teacher) {
        return teacherRepositery.save(teacher);
    }

    @Override
    public void deleteTeacher(int id) {
        teacherRepositery.deleteById(id);

    }
}
