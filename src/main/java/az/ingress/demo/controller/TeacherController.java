package az.ingress.demo.controller;

import az.ingress.demo.model.Teacher;
import az.ingress.demo.service.TeacherService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.*;

@RequiredArgsConstructor
@RestController
@RequestMapping("/teacher")
public class TeacherController {

    private final TeacherService teacherService;

    @GetMapping ("/{id}")
    public Teacher getTeacherbyId (@PathVariable int id){
        return teacherService.getById(id);
    }

    @PostMapping
    public Teacher saveTeacher(@RequestBody Teacher teacher){
        return teacherService.saveTeacher(teacher);}

@PutMapping
    public Teacher updateTeacher(@RequestBody Teacher teacher){
        return teacherService.updateTeacher(teacher);
}

@DeleteMapping
    public String deleteId(@RequestParam(value="id") int id){
        teacherService.deleteTeacher(id);
        return "deleted by id : " + id;

}
}
