package az.ingress.demo.controller;

import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping(path="/api")
public class HelloController {

    @GetMapping
    public  String sayHello(){

        return "Hello Word";
    }
    @RequestMapping(path= "/test",method = RequestMethod.GET )
    @GetMapping("/test")
    public String talk(){
        return "Ingress";
    }
    @GetMapping("/{get}")
    public String foo(@PathVariable ("get") String test){
        return "Hello Mustafa";
    }
    @PostMapping("/save")
    public String save(){
        return "Date saved";
    }
    @DeleteMapping("/delete ")
    public String delete(){
        return "Data Deleted";
    }


}
