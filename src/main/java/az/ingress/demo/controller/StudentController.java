package az.ingress.demo.controller;

import az.ingress.demo.model.Student;
import az.ingress.demo.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;



@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

private final StudentService studentServis;

    @GetMapping("/{id}")
    public Student getStudentById(@PathVariable int id) {
        return studentServis.getStudentById(id);
    }
    @PostMapping
    public Student saveStudent(@RequestBody Student student){
        return studentServis.saveStudent(student);
    }

    @PutMapping
    public Student updateStudent(@RequestBody Student student){
        return studentServis.updateStudent(student);
    }
    @DeleteMapping("/{id}")
    public String deleteStudent(@PathVariable int id){
        studentServis.deleteStudent(id);
        return "Deleted student by : " + id;
    }

}
